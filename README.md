# Описание:

Эта программа призвана упростить работу с кафкой пользователям, которые не особо погружены в ИТ или ищут простой инструмент с web-интерфейсом.  
### Используются следующие технологии:
* Spring-Boot
* Spring-Web
* Freemarker (при желании можно заменить на angular 8 или иное) 
* Spring-Kafka

### How to run
1. Pull image - `docker pull 89046020956/kafka-test-tool:deploy`
2. Start docker container** - `docker run -p 5000:5000 89046020956/kafka-test-tool:deploy` 
3. Open WebUI - [http://localhost:5000/](http://localhost:5000/)

** mount config (see [Configuration](#Configuration))

### Configuration
Example of application config locates in docker container by path `/app/application.properties.example`. Need to remove `.example` from filename and set your values.


### Инструкции по совместной работе
[Канбан доска с задачами](https://github.com/Star-forge/kafka-test-tool/projects/1). Работа ведется  по задачам с доски. Можно добавлять любые задачи на доску. Желательно разбивать и формулировать задачи так, чтобы на них уходило до 25 минут. Рекомендуемое время 15 минут.   
[Сборочный pipeline](https://github.com/Star-forge/kafka-test-tool/actions) покажет ломает ли ваш пуш приложение.  
Работа над задачей происходит в отдельных ветках. Именование веток свободное.   
Все изменения вливаются в общую ветку посредством [pull request](https://github.com/Star-forge/kafka-test-tool/pulls)  
[Чатик на Gitter](https://gitter.im/star-forge/community)

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/maven-plugin/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring for Apache Kafka](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-kafka)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

