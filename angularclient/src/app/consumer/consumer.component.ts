import {Component, OnInit} from '@angular/core';
import {ConsumerService} from "../services/consumer/consumer.service";
import {Observable, timer} from "rxjs";
import {ProducerService} from "../services/producer/producer.service";
import {DictService} from "../services/dict/dict.service";

@Component({
  selector: 'app-consumer',
  templateUrl: './consumer.component.html',
  styleUrls: ['./consumer.component.css', '../app.component.css']
})

export class ConsumerComponent implements OnInit {
  topic: String = 'topicName';
  messages: String = "";
  group: String = "";

  constructor(private rest: ConsumerService, private dict: DictService) {
  }

  ngOnInit() {
    this.setTopic();
    this.setGroup();

    timer(0, 2000)
      .forEach(_ => this.getMessages());
  }

  private setTopic() {
    this.dict.getTopic().subscribe(
      next => this.topic = next.IN,
      error => console.error(error));
  }

  private setGroup() {
    this.dict.getGroup().subscribe(
      next => this.group = next.GROUP,
      error => console.error(error));
  }

  private getMessages() {
    this.rest.getMessages().subscribe(
      (next: String[]) => this.messages += next.length > 0 ? next.join("\n")+"\n" : "",
      error => console.error(error)
    );
  }

  clearTextArea() {
    this.messages = "";
  }
}
