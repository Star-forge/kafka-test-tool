import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GsettingsComponent} from './gsettings.component';

describe('GsettingsComponent', () => {
  let component: GsettingsComponent;
  let fixture: ComponentFixture<GsettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GsettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GsettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
