import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-gsettings',
  templateUrl: './gsettings.component.html',
  styleUrls: ['./gsettings.component.css']
})
export class GsettingsComponent implements OnInit {
  checked = true;
  indeterminate = false;

  constructor() { }

  ngOnInit() {
  }

}
