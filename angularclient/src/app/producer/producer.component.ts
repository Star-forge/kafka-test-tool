import {Component, OnInit} from '@angular/core';
import {ProducerService} from "../services/producer/producer.service";
import {DictService} from "../services/dict/dict.service";

@Component({
  selector: 'app-producer',
  templateUrl: './producer.component.html',
  styleUrls: ['./producer.component.css', '../app.component.css']
})
export class ProducerComponent implements OnInit {
  topic: String = "";
  messages: String = "";
  isOneObject: boolean = false;

  constructor(private rest: ProducerService, private dict: DictService) {
  }

  ngOnInit() {
    this.dict.getTopic().subscribe(
      next => this.topic = next.OUT,
      error => console.error(error))
  }

  send() {
    if (this.isOneObject)
      this.sendMsgs([this.messages]);
    else if (this.messages.length > 0) {
      this.sendMsgs(this.messages.length > 0 ? this.messages.split("\n") : ["\n"])
    }
  }

  private sendMsgs(msgs: String[]) {
    this.rest.sendMessages(msgs).toPromise().then(_ => console.log("sended"));
  }
}
