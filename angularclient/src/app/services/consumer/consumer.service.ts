import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ConsumerService {

  constructor(private http: HttpClient) {
  }

  getMessages() : Observable<String[]>{
    return  this.http.get<String[]>(environment.endpoint + 'msg/');
  }
}
