import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DictService {

  constructor(private http: HttpClient) {
  }

  getTopic() {
    return this.http.get<Topics>(environment.endpoint + 'topics/');
  }

  getGroup(): Observable<any> {
    return this.http.get<Group>(environment.endpoint + 'group/');
  }
}

interface Topics {
  readonly OUT: String;
  readonly IN: String;
}

interface Group {
  readonly GROUP: String;
}
