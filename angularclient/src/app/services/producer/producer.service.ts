import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProducerService {

  constructor(private http: HttpClient) {
  }

  sendMessages(msgs: String[]): Observable<any> {
    // TODO: use routing

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(environment.endpoint + 'msg', JSON.stringify(msgs).toString(), {headers})
  }


}
