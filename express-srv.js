const express = require('express');
const proxy = require('express-http-proxy');
const app = express();
const PORT = 5000;
const HOST = '0.0.0.0';

app.use(express.static('./'));

const apiProxy = proxy('http://localhost:8080', {
    proxyReqPathResolver: function (req) {
        return req.url;
    }
});

app.all('/rest/*', apiProxy);

app.listen(PORT, HOST, () => `Running on http://${HOST}:${PORT}`);
