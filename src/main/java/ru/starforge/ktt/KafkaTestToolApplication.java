package ru.starforge.ktt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTestToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaTestToolApplication.class, args);
    }

}
