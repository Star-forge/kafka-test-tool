package ru.starforge.ktt.kafka.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author starforge
 */
@Component
public class StringConsumer {
    private static final Logger log = LoggerFactory.getLogger(StringConsumer.class);

    @Value("${buffer.size}")
    private int size = 1;

    private List<String> buffer = new ArrayList<>();

    public List<String> getMessages() {
        List<String> msgs = new ArrayList<>(buffer);
        buffer.clear();
        return msgs;
    }

    @KafkaListener(topics = "${spring.kafka.consumer.topic}")
    public void processMessage(String value) {
        while (buffer.size() >= size) ;
        log.debug("received message: '{}'", value);
        buffer.add(value);
    }
}
