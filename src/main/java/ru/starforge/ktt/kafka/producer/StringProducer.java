package ru.starforge.ktt.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

/**
 * @author starforge
 */
@Component
public class StringProducer {
    private static final Logger log = LoggerFactory.getLogger(StringProducer.class);

    @Value("${spring.kafka.producer.topic}")
    String producerTopic;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public StringProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public boolean sendMessage(String msg) {
        log.debug("Send message {} in topic {}", msg, producerTopic);
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(producerTopic, msg);
        return future.isDone();
    }
}
