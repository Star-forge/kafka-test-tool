package ru.starforge.ktt.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.starforge.ktt.kafka.consumer.StringConsumer;

import java.util.List;

/**
 * @author starforge
 */
@RequestMapping("/rest")
@RestController
@CrossOrigin(origins = "http://localhost:5000")
public class ConsumerRestController {
    final StringConsumer consumer;

    public ConsumerRestController(StringConsumer consumer) {
        this.consumer = consumer;
    }

    @GetMapping(path = "/msg")
    public ResponseEntity<List<String>> getMessages() {
        return ResponseEntity.ok(consumer.getMessages());
    }
}
