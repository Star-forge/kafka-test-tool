package ru.starforge.ktt.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/rest")
@RestController
@CrossOrigin(origins = "http://localhost:5000")
public class DictRestController {

    @Value("${spring.kafka.consumer.topic}")
    private String consumerTopic = "";

    @Value("${spring.kafka.producer.topic}")
    private String producerTopic = "";

    @Value("${spring.kafka.consumer.group-id}")
    private String consumerGroup = "";

    @GetMapping(path = "/topics")
    public ResponseEntity<Map<String, String>> getTopics() {
        Map<String, String> topics = new HashMap<>();
        topics.put("CONSUMER", consumerTopic);
        topics.put("PRODUCER", producerTopic);
        return ResponseEntity.ok(topics);
    }

    @GetMapping(path = "/group")
    public ResponseEntity<Map<String, String>> getConsumerGroup() {
        Map<String, String> response = Collections.singletonMap("GROUP", consumerGroup);
        return ResponseEntity.ok(response);
    }
}
