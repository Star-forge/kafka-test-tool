package ru.starforge.ktt.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.starforge.ktt.kafka.producer.StringProducer;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author starforge
 */
@RequestMapping("/rest")
@RestController
@CrossOrigin(origins = "http://localhost:5000")
public class ProducerRestController {

    final StringProducer producer;

    public ProducerRestController(StringProducer producer) {
        this.producer = producer;
    }

    @PutMapping("/msg")
    public ResponseEntity<Object> putMessage(@RequestParam String msg) {
        if (producer.sendMessage(msg))
            return ResponseEntity.accepted().build();
        else
            return ResponseEntity.status(500).build();
    }

    @PostMapping("/msg")
    public ResponseEntity<Object> postMessages(@RequestBody List<String> msgs) throws InterruptedException, ExecutionException, TimeoutException {
        for (String msg : msgs) {
            producer.sendMessage(msg);
        }
        return ResponseEntity.accepted().build();
    }
}
